---
title: Alienation
---

# Capitalist Alienation

Alienation is the result of working and social conditions under capitalism. 
Society, culture, and ideology all have a role in the removal of the individual
from the fruits of their industry, enjoyment of the world, interactions with 
others and sense of accomplishment. The continuous evolution of capitalism
makes all these disparate concepts feel removed from their source, which is 
the commodification of the individual and the fruits of their labor.

The worker doesn't feel as attached to the result of their work. Creations are
owned by the corporation (business owner) rather than the individual that 
created them. Due to occult mental gymnastics (capitalist ideology) the owner 
then profits off the creations of their workers by divorcing their natural
ownership of creations. Let me pay for your labor and everything you create is
mine to sell and profit from.

Competition between individuals alienates workers from each other. As long as 
someone is seen as competition they are less likely to work together or engage 
in mutual aid. Coworkers become potential liabilities rather than comrades. 
Even neighbors are potential rivals in the labor market. 

Distance from nature has created an impasse between the animals that make up 
meat products, enjoyment of the outdoors, and exploitation of resources. A
chicken nugget, burger paddy, or sausage is now so far removed from the body it
originated that not all people even recognize what animal it came from. This 
helps conceal conditions these animals are kept in and the horrific processes
of butchering. Cities are large enough to be concrete jungles and remove any 
natural vegetation from an area. Pollution and mining destabilize any natural 
serenity from an area.

Alienation from the self is the most weird type of alienation. Works produced
by the individual are no longer attributed to their creator but to some brand
or organization. Any sense of worth from the act of creation is removed and its
value replaced by a sticker price. No meaning is gained for the creator and 
their sense of self diminished.

Capitalist evolution is more of a segue into different forms of appropriation.
Including art, history, reproduction, education and anything else that has 
become a commodity.

## Resources

* [wiki: Marx's theory of alienation](https://en.wikipedia.org/wiki/Marx%27s_theory_of_alienation)
* [plasticpills: youtube breakdown](https://www.youtube.com/watch?v=gPRxxN4Kh30)

---
title: 'The Attention Economy - the destruction of free will'
---

# Attention

Beginning in this new millennia was a phenomenon that could well be humanity's
greatest challenge. Artificial systems and networks have conspired to
manipulate and control every person on the planet. These systems operate
entirely without sympathy, remorse, or conscience. Networks such as Facebook,
Imagur, Tiktok, and google develop persuasive techniques with learning
algorithms to control and manipulate users of their software. All in the
pursuit of more advertising revenue. The optimization of these systems is
done be the learning machines themselves, with great help from engineering.

The silent war for attention has already been won. As a biological
organism there is only so much resistance that can be mustered against a
threat that persuades instinctual and evolutionary mechanisms. The great
apes are evolved to be social creatures reliant on keeping and maintaining
reputations within tribal systems. This has proven to be one of our easiest
weaknesses to subvert. Fear of losing out on popularity, social hierarchy,
and prestige. The less political influence available for a member of a tribe
the more alienated and stressed they become.

Social networks exist to sell advertisements. These advertisement can be used
for various ends. Products, influence, and truth are all for sale. This is
detrimental to users of these applications. Overcoming these disgusting,
perverse manipulators is the trial of our lifetimes.

### More reading

* [Social media marketing](https://en.wikipedia.org/wiki/Social_media_marketing)
* [Realtime ad auction](https://www.facebook.com/business/ads/pricing?_fb_noscript=1)


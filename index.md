---
title: Various Rants!
---

# Greetings and welcome

This website is dedicated to various rants and hopefully nothing
else. Everything is overtly political and there are no facts. Enjoy your
stay and try to rant as well.

### Content:
* [Alienation](/alienation.html)
    * Capitalism and removal of the self, other, product, and nature
* [Attention Economy](/attention_economy.html)
	* Attention Economy - propaganda and manipulation
* [All Against All](/all_against_all.html)
	* The war of all against all - the Leviathan
* [Uprising](/uprising.html)
  * Police violence and media narrative
* [Pandemic](/pandemic.html)
	* Misinformation lies and deciet
* [Max Stirner](/stirner.html)
	* Dispeller of ideas
* [Tribalism](/power_elite_metoo.html)
	* Factions and the capacity for hipocracy
* [Countries](/the_us_isnt_real.html)
	* Giving something a name doesn't make it real
* [Purpose of a System](/purpose_of_a_system.html)
	* Creating myths and control of group members


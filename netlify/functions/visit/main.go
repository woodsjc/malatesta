package main

import (
	"context"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jackc/pgx/v4"
)

func handler(context context.Context, request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	pg, err := pgx.Connect(context, os.Getenv("DB_STRING"))
	if err != nil {
		return nil, err
	}
	defer pg.Close(context)

	insertQuery := `insert into netlify.visits (ip, request_headers) values ($1, $2);`
	ip, ok := request.Headers["x-forwarded-for"]
	if !ok {
		ip = ""
	}
	_, err = pg.Exec(context, insertQuery, ip, request.Headers)
    if err != nil {
        return nil, err 
    }

	body := ""
	for k, v := range request.Headers {
		body += fmt.Sprintf("%s: %s\n", k, v)
	}

	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       body,
	}, nil
}

func main() {
	lambda.Start(handler)
}

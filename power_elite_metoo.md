---
title: He who has the gold makes the rules
---

# He who has the gold makes the rules

If there is anything to learn from the main contenders in the two most popular us political parties, it's the danger of tribalism. Allegations of sexual misconduct have come for both Trump and Biden neither of which have ever presented anything resembling character. These men are the epitome of power without consequence. If you take care of your party it will take care of you. Don't worry about anything but being a team player. Us vs Them.

The adherents of a tribal faction don't realize they are being conned. The individuals that run the party will move their own agenda forward while demonizing the other faction. Believers are fanatically belligerent against the other faction and completely ignore the money being taken out of their own pocket.

This pathetic state of human instinct can only be overcome by critical thinking. Do not believe what comes from an authority without justification. There are many ways to make an argument and most of the debates that take place involve one logical fallacy after another. It's time to move on.


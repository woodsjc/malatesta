---
title: The Purpose of a System is What it Does
---

# The purpose of a system is what it does

Systems like to create beautiful mythologies of their origins, history and 
motivations. These stories help members of a system conform to a expectations 
within the system and foster group think. These stories have little to do 
with reality, but do have a lot to do with control of the group.

The results of a system\'s actions are in fact what it\'s purpose is. The 
founding myths are integral in creating complicit workers. Without these 
myths everyone is left to their own stories of origins and motivations. A 
fractured origin story is not a good basis for social cohesion. The group 
members are agents of their own fantasy rather than one provided for them.

## Reflection

This view of a system really doesn\'t provide a complete picture. There are 
obviously changes in systems over time in relation to the external world and 
internal pressure on the system. It must at all time be composed of agents who
don\'t have the same views, beliefs, motivation or alignment. The agents are 
the only ones capable of morality. Systems are entirely ethereal, they just seem
like good targets to anthropomorphize.

## Further reading

* [wiki](https://en.wikipedia.org/wiki/The_purpose_of_a_system_is_what_it_does)

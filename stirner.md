---
title: 'Max Stirner and the revelation of the spook:'
---

# Stirner

Recognizing ideas as spooks, Stirner elucidated the problems with ideas having
no physical manifestation. Ideas are not simply models of the real world. They
have the power to create action. A believer can act in accordance with the best
interest of an idea rather than their own interest. This disassociation between
an individual and their self interest breeds malcontent.

Ideas and concepts certainly have their place. The world can be sliced and
filtered into concepts that provide an advantage to those who understand their
meaning. Knowing where a location is relative to another means travel between
the two is possible. Conjuring maps and schematics from ideas and concepts has
fostered construction, development, and intuition.

Disconnected ideas control and manipulate. God, the state, social constructions,
are all manipulative ideas devoid of any real meaning. These are the
quintessential tools of charlatans and scammers. But they are used by
individuals within social organizations to project power of something that
doesn't exist or justify structures of power already in place. Our minds are all
too willing to then create this nonexistent entity, giving it power and
spreading influence.

Choose not to believe in corrupt ideas and life is much simpler. People create
meaning for themselves or allow others to create leviathans. 

### More reading

* [The Ego And His Own](https://theanarchistlibrary.org/library/max-stirner-the-ego-and-his-own)
* [Wiki - Max Stirner](https://en.wikipedia.org/wiki/Max_Stirner)

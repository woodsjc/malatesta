---
title: 'The United States of America isn''t real.'
---

# This country isn't real

Look for it in the land it claims to own and you cannot find it.

Look for it in the people that dwell within it and you cannot find it.

Look for it in buildings that it owns and you cannot find it.

Look for it in a flag and you cannot find it.

Look for it in the capital and you cannot find it.

Look for it in the White House and you cannot find it.

## Ideas aren't manifest

People create ideas and communicate their definitions. This act of
communication does not generate reality. The actual world does not consist of
ideas. Only a person's interpretation of the world contains these mysterious
relics of communication.


